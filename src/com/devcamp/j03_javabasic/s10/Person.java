package com.devcamp.j03_javabasic.s10;
import java.util.Arrays;
public class Person {
    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + salary 
        + ", pets=" + Arrays.toString(pets) + "]";
    }

    String name;// tên người
    int age;// tuổi
    double weight;// cân nặng
    long salary;// lương
    String[] pets;// vật nuôi
    //Khởi tạo 1 tham số name
    public Person(String name) {
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 10000000;
        this.pets = new String[] { "Big Dog", "Small Cat", "Gold Fish", "Red Parrot" };
    }
    //Khởi tạo với tất cả tham số
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.pets = pets;
        this.salary = salary;
    }
    //Khởi tạo không tham số
    public Person() {
        this("HieuHN");
    }
    //Khởi tạo với 3 tham số
    public Person(String name, int age, double weight) {
        this(name, age, weight, 20000000, new String[] { "Big Dog", "Small Cat", "Gold Fish", "Red Parrot" });
    }
}
